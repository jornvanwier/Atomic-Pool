# Atomic Pool
Atomic Pool is a school project for Graphics 1. We had to build a pool like game using web technologies like JavaScript and ThreeJS.

# Play
The game can be played at https://jornvanwier.gitlab.io/Atomic-Pool/.